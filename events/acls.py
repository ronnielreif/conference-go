from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):

    url = f"https://api.pexels.com/v1/search?query={city} {state}"

    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=headers)

    api_dict = response.json()

    return api_dict["photos"]["src"]["original"]



def get_weather(city, state):
    geocoding_url = f"https://api.openweathermap.org/data/2.5/weather?q={city},{state}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(geocoding_url)
    api_dict = response.json()

    if "coord" not in api_dict:
        return None
    lat = api_dict["coord"]['lat']
    lon = api_dict["coord"]['lon']

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weath_response = requests.get(weather_url)
    weather_dict = weath_response.json()

    main_temp = weather_dict["main"]["temp"]
    weather_description = weather_dict['weather'][0]['description']

    temp_dict = {
        "temperature": main_temp,
        "description": weather_description
    }
    return temp_dict
